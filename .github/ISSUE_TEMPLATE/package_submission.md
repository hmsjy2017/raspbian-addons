---
name: Package submission
about: Submit a package to possibly be added to the repository.
title: ''
labels: package submission
assignees: ''

---

Describe what the software is here. Will many people use it? What is its main purpose?

Licensing: What license does the software have? Is it free software? Is it allowed to be distributed?

If you have a .deb file, upload it here. If not, please provide a link to the software or its distrubutables (packages).
